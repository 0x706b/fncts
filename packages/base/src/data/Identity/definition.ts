/**
 * @tsplus type fncts.Identity
 */
export type Identity<A> = A;

/**
 * @tsplus type fncts.IdentityOps
 */
export interface IdentityOps {}

export const Identity: IdentityOps = {};
